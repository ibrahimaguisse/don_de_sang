//jshint ignore:start 
import React from 'react';
import './App.css';
import axios from "axios";

class App extends React.Component
{
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: true,
      features:[]
      
    };
  }
  componentDidMount() {
    axios.get('http://api.openeventdatabase.org/event/?what=health.blood.collect&when=nextweek&limit=1000',{method:'GET'})
      //.then(res => res.json())
      .then(
        (res) => {
          this.setState({
            
            isLoaded:false,
            features:[]  
          });
        },
        // Remarque : il est important de traiter les erreurs ici
        // au lieu d'utiliser un bloc catch(), pour ne pas passer à la trappe
        // des exceptions provenant de réels bugs du composant.
        (error) => {
          this.setState({
            isLoaded:true,
            
            
          });
        }
      )
  }

  render()
  {
    const { error, isLoaded,features} = this.state;
 
      if (error) {
        return (
          <div className="App">
            Error: {error.message}
          </div>
        );
      } else if (!isLoaded) {
        return (
          <div className="App">
            Loading...
            
          </div>
        );
      } else {
        
    return (
      <div className="App">
        <h1>
            Application listant les lieux de collectes du don de sang 
          </h1>
          
        
        <ul>
          {features.map( features => (
            <li key={this.state.features}>
              {this.props.features} {features.limit}
            
            </li>
          ))}
        </ul>
      </div>
    );
          }
  }

}
export default App;