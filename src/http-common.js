//jshint ignore:start
import axios from "axios";

export default axios.create({
  baseURL: "http://localhost:3000/api.openeventdatabase.org/event/?what=health.blood.collect&when=nextweek&limit=1000",
  headers: {
    "Content-type": "application/json"
  }
});