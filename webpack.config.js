//jshint ignore:start 
const  webpack  =  require ( 'webpack' ) ; 
const  _  =  require ( 'lodash' ) ; 
const  WebpackNotifierPlugin  =  require ( 'webpack-notifier' ) ; 
const  WebpackShellPlugin  =  require ( 'webpack-shell-plugin' ) ; 
const  FilterWarningsPlugin  =  require ( 'webpack-filter-warnings-plugin' ) ;

module . exports  =  ( { config } )  =>  { 
  config . résoudre . extensions . push ( '.ts' ,  '.tsx' ,  '.js' ,  '.jsx' ,  '.json' ) ; 
  config . module . règles . push ( 
    { 
      test : / \. tsx? $ / , 
      exclure : / node_modules / , 
      utilisez :[ 
        { 
          Chargeur : 'babel-loader' , 
          Options : {  CACHEDIRECTORY : true  } 
        } , 
        { 
          chargeur : 'ts-loader' , 
          Options : {  transpileOnly : true  } 
        } 
      ] 
    } ,